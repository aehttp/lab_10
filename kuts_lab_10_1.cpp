/*file name: kuts_lab_10_1 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 15.12.2021
*дата останньої зміни 15.12.2021
*Лабораторна №10
*завдання : Розробити блок-схему алгоритму та реалізувати його мовою С\С++ для обробки слова або символьного рядка 
*призначення програмного файлу: Визначити кількість входжень у рядок s довільного символу, пошук здійснювати із заданого індексу.
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	string a;
	char b;
	int i ,s , k, n=0;
	printf("Введіть рядок s: \n");
	getline(cin,a);
	printf("Введіть символ: \n");
	cin>>b;
	printf("Введіть індекс початку пошуку: \n");
	cin>>k;
	s=a.length();
	for(i=k; i<s;i++){
		if(a[i] == b){
			n++;
		}
	}
	printf("Кількість входжень у рядок символа %c : %4d \n",b,n);
	system("pause");

	return 0;
}

