/*file name: kuts_lab_10_2
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 15.12.2021
*дата останньої зміни 15.12.2021
*Лабораторна №10
*завдання : Розробити блок-схему алгоритму та реалізувати його мовою С\С++ для обробки слова або символьного рядка 
*призначення програмного файлу: В заданому тексті знайти і вивести кількість цифр і кількість букв.
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	string s;
	string b="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZабвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯіїяІЇҐ";
	string c="0123456789";
	int i,j, bk=0,ck=0;
	printf("Введіть текст: \n");
    getline(cin,s);
    for(i=0;i < s.length();i++){
    	for(j=0;j<b.length();j++){
    		if(s[i] == b[j])
    			bk++;
    	}
    	for(j=0;j<c.length();j++){
    		if(s[i] == c[j])
    			ck++;
    	}
    }
    system("cls");
    printf("Сформований текст: \n");
	cout<<s<<endl;
    printf("Кількість букв = %4d \n",bk);
    printf("Кількість цифр = %4d \n",ck);	
	system("pause");
	return 0;
}

